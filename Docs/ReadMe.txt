*���������what are the similarities and differences between model and code?
�The diagram uses a more case scenario when the application is running.  It�s more of the overview of what the app should do in the final case.  But the code is more intricate and much more complex.  You also implement more in the code than the overview of the model.

Some similarities are calling the same methods.  You can see from the model and the code with what methods get called at certain times during the application.  It also shows a nice overview of how the code should be factored.  You can figure out how to separate the code by looking at the model you created.  
�


*���������Is it more effective to code or model first?

We believe that doing a model first would have helped our first project out significantly.� Seeing how the related fields describe the behavior of the system allows us to code more efficiently.� If we ended up modeling first we are able to keep track of the methods and the relationships between certain classes.� After we modeled our program it was easy to decipher how a Click timer should be built.�
�
�
�
*���������Now that you have a model, are there any changes you would want to make to your code?

Yeah noticing the model that we created now, I can see that we weren�t using the model effectively.� It seemed to be a much harder project than we anticipated.� We are going back and going to make a few changes to our Project 1a, too make sure it works more efficiently.

