/**
	 * Not sure if we needed this.
	 * I was using it to show what that minimun and the maximum is for the CountDownTimer.
	 * And declare all the methods.
	 * Seemed like we don't need this, but I want you to see what some of my coding was.
	 * I was basing it off of what you had on the clickCounter Example
	 */




package com.example.cs313f12p1a.model;

import android.widget.TextView;

public class DefaultBoundedCounter implements WaveInterface {
	
	private final int min;
	private TextView timedisplay;
//	/**
//	 * The upper bound of the counter.
//	 */
	
	private final int max;

//	/**
//	 * The current value of the counter.
//	 */
	//	
	private int value;
	private int millisUntilFinished;

	//	/**
	//	 * Constructs a bounded counter with the default bounds.
	// */
	public DefaultBoundedCounter() {
			this(0, 99);
		}
	
	


	public DefaultBoundedCounter(final int min, final int max) {
				if (max >= 100) {
			throw new IllegalArgumentException("Max too high!");
					}
				this.min = min;
				this.max = max;
				this.value = this.min;
			}
	



	public void onTick() {
		timedisplay.setText("Time Left: " + millisUntilFinished / 1000);
		
	}

	public void onFinish() {
		// TODO Auto-generated method stub
		
	}

	public void onStop() {
		// TODO Auto-generated method stub
		
	}
	

	
	
	
	
}
