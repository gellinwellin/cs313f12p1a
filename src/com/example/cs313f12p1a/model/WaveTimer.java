/**
	 * Not sure if we needed this.
	 * I was using it to show what that minimun and the maximum is for the CountDownTimer.
	 * And declare all the methods.
	 * Seemed like we don't need this, but I want you to see what some of my coding was.
	 * I was basing it off of what you had on the clickCounter Example
	 */




package com.example.cs313f12p1a.model;

import android.os.CountDownTimer;
import android.widget.TextView;

	public abstract class WaveTimer extends CountDownTimer implements WaveInterface {

		public long millisInFuture;
		private long millisUntilFinished;
		private long countDownInterval;
		private TextView timedisplay;
		private WaveTimer timer;
		
		public WaveTimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			timedisplay.setText("Countdown Finished");
			timer.cancel();
		}
		

		public void onStop() {
			millisInFuture = millisInFuture + 1;
			timedisplay.setText("Time Left: " + millisInFuture);
		}

		@Override
		public void onTick(long millisUntilFinished) {
			millisUntilFinished--;
			if (millisUntilFinished == 0){
				timer.onFinish();
			}
			else {
			timedisplay.setText("Time Left: " + millisUntilFinished / 1000);
			
		}
}
	}