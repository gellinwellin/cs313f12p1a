package com.example.cs313f12p1a.model;

public interface WaveInterface {
	 
	 /**
		 * Displays finishing text
		 */
	
	 void onFinish();
	 
	 /**
		 * Stops the process.
		 */
	 void onStop();
	 
	 
	 
	 void onTick();
	 
	 
}
