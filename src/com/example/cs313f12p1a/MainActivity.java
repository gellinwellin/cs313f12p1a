package com.example.cs313f12p1a;

import com.example.cs313f12p1a.model.WaveInterface;


import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

@SuppressWarnings("unused")
public class MainActivity extends Activity {
	
	private static String TAG = "cs313f12p1a";
	/**
	 * Setter for the model.  Didn't Use correctly.  Need to figure out.
	 */
//	public void setModel(final WaveInterface model) {
//		this.model = model;
//	}
	
	/**
	 * Declaring variables for main activity
	 */
        Button incrementTime, startTime;
        Button button2, button3;
        public TextView timedisplay;
        public myTimer wavetimer;
    	private long millisInFuture;
    	private long millisUntilFinished;
    	private long countDownInterval;
    	private long onclicktime;
    	private WaveInterface model;
    	public int countdown;
    	
    	
    	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);     
        incrementTime = (Button) findViewById(R.id.button2);
        startTime = (Button) findViewById(R.id.button3);
        incrementTime.setText("Increment Time");
        startTime.setText("Start");
        timedisplay = (TextView) findViewById(R.id.mycounter);
        timedisplay.setText("Time Left: " + millisUntilFinished);
        wavetimer = new myTimer (countdown, 1000);
        
        
        incrementTime.setOnClickListener(new OnClickListener(){
        int countdown = 01;
       /**
        * On click button Listner
     * @return 
        */
		public void onClick(View v) {
			countdown++;
			return;	
		}
		  
       });
        startTime.setOnClickListener(new OnClickListener(){
        public void onClick(View v) {
        	wavetimer.start();
        }
    });}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
/**
* myTimer class, declaring the methods.
*/

public class myTimer extends CountDownTimer  {
	public long millisInFuture;
	private long countDownInterval;


	public myTimer(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
	}
	@Override
	public void onTick(long millisUntilFinished) {
		millisUntilFinished--;
		if (millisUntilFinished == 0){
			wavetimer.onFinish();
		}
		else {
		timedisplay.setText("Time Left: " + millisUntilFinished / 1000);
		}
	
	}
	@Override
	public void onFinish() {
		timedisplay.setText("Countdown Finished");
		wavetimer.cancel();
	}
	

	public void onStop() {
		millisInFuture = millisInFuture + 1;
		timedisplay.setText("Time Left: " + millisInFuture);
	}

		
		

} 

}





//package com.example.cs313f12p1a.android;
//
//import com.example.cs313f12p1a.android.*;
//import com.example.cs313f12p1a.model.WaveInterface;
//import com.example.cs313f12p1a.model.WaveTimer;
//
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.os.CountDownTimer;
//import android.view.Menu;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.widget.Button;
//import android.widget.TextView;
//
//@SuppressWarnings("unused")
//public class MainActivity extends Activity {
//	
//	private static String TAG = "cs313f12p1a";
//	/**
//	 * Setter for the model.  Didn't Use correctly.  Need to figure out.
//	 */
////	public void setModel(final WaveInterface model) {
////		this.model = model;
////	}
//	
//	/**
//	 * Declaring variables for mainactivity
//	 */
//        Button incrementTime;
//        public TextView timedisplay;
//    	private long millisInFuture;
//    	private long millisUntilFinished;
//    	private long countDownInterval;
//    	private long onclicktime;
//    	private WaveInterface model;
//    	protected WaveTimer timer;
//    	
//    	
//    	
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);     
//        incrementTime = (Button) findViewById(R.id.button2);
//        incrementTime.setText("Stop Timer");
//        timedisplay = (TextView) findViewById(R.id.mycounter);
//        timedisplay.setText("Time Left: " + millisUntilFinished);
//        timer = new WaveTimer (10000, 1000);
//        incrementTime.setOnClickListener(new OnClickListener(){
//        
//        	
//       /**
//        * On click button Listener
//        */
//		public void onClick(View v) {
//
//		}   
//    	   
//       });
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_main, menu);
//        return true;
//    }
//
//
//
//}
//
//
